﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using Models;
using Newtonsoft.Json;

namespace Server.Data
{
    /// <summary>
    /// not a thread safe thing. could produce dirty writes. use <see cref="CommitChangesAsync"/> to write.
    /// i won't be writing a fully consistent json database here, so it is as it is.
    /// </summary>
    public class RecordsRepository : IRecordsRepository
    {
        private readonly string _path = Path.Combine(Path.GetDirectoryName(Assembly.GetEntryAssembly().Location), "records.json");
        private List<RecordModel> _records;

        public RecordsRepository()
        {
            if (!File.Exists(_path)) // touching a "records.json" file
                using (new FileStream(_path, FileMode.OpenOrCreate)) ;
        }

        public async Task<IQueryable<RecordModel>> GetRecordsAsync()
        {
            await ReadRecordsIfDefault();

            return _records.AsQueryable();
        }

        public async Task AddRecordAsync(RecordModel record)
        {
            await ReadRecordsIfDefault();

            if (_records.Any(item => item.Id == record.Id))
                throw new InvalidOperationException($"cannot add record with Id={record.Id}, which already exist");

            _records.Add(record);
        }

        public async Task UpdateRecordAsync(RecordModel record)
        {
            await ReadRecordsIfDefault();

            var recordToUpdate = _records.SingleOrDefault(item => item.Id == record.Id);
            if (recordToUpdate == default)
                throw new InvalidOperationException($"cannot update record with Id={record.Id}, which does not exist");

            // no Id, because Id is kind of get-only thing after record has been created
            recordToUpdate.Image = record.Image;
            recordToUpdate.Name = record.Name;
        }

        public async Task DeleteRecordAsync(Guid id)
        {
            await ReadRecordsIfDefault();

            var recordToRemove = _records.SingleOrDefault(item => item.Id == id);
            if (recordToRemove == default)
                throw new InvalidOperationException($"cannot remove record with Id={id}, which does not exist");

            _records.Remove(recordToRemove);
        }

        public async Task CommitChangesAsync()
        {
            var strb = new StringBuilder();
            foreach (var record in _records)
            {
                strb.AppendLine(JsonConvert.SerializeObject(record));
            }

            await File.WriteAllTextAsync(_path, strb.ToString());
        }

        /// <summary>
        /// this thing is kind of lazy load, it fires only after any of public methods is called
        /// </summary>
        /// <returns></returns>
        private async Task ReadRecordsIfDefault()
        {
            _records ??= await ReadRecordsAsync();
        }

        private async Task<List<RecordModel>> ReadRecordsAsync()
        {
            var lines = await File.ReadAllLinesAsync(_path);
            var list = new List<RecordModel>();
            foreach (var line in lines)
            {
                list.Add(JsonConvert.DeserializeObject<RecordModel>(line));
            }

            return list;
        }
    }
}