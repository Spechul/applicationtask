﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Models;

namespace Server.Data
{
    /// <summary>
    /// kind of Repository + Unit of work, like EF core's AppDbContext is
    /// </summary>
    public interface IRecordsRepository
    {
        Task<IQueryable<RecordModel>> GetRecordsAsync();
        Task AddRecordAsync(RecordModel record);

        Task UpdateRecordAsync(RecordModel record);

        Task DeleteRecordAsync(Guid id);
        Task CommitChangesAsync();
    }
}