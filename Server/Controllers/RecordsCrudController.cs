﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Models;
using Server.Data;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace Server.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class RecordsCrudController : ControllerBase
    {
        private IRecordsRepository _recordsRepository;
        public RecordsCrudController(IRecordsRepository repository)
        {
            _recordsRepository = repository;
        }

        // GET: api/RecordsCrud
        [HttpGet]
        public async Task<IActionResult> Get()
        {
            var records = await _recordsRepository.GetRecordsAsync();
            return new JsonResult(records.ToArray());
        }

        // GET api/RecordsCrud/a149304d-c419-4adb-986b-32af74f2de42
        [HttpGet("{id}")]
        public async Task<IActionResult> Get(Guid id)
        {
            var records = await _recordsRepository.GetRecordsAsync();
            var record = records.SingleOrDefault(item => item.Id == id);
            if (record == default)
                return NotFound($"no such a record with Id={id} exist");

            return new JsonResult(record);
        }

        // POST api/RecordsCrud/ea3fddd3-ff99-4974-a053-fdd6f0d21ff7
        [HttpPost("{id}")]
        public async Task<IActionResult> Post(Guid id, [FromBody] CutRecordModel value)
        {
            var record = new RecordModel
            {
                Id = id,
                Name = value.Name,
                Image = value.Image
            };

            await _recordsRepository.UpdateRecordAsync(record);
            await _recordsRepository.CommitChangesAsync();
            return Ok();
        }

        // PUT api/RecordsCrud
        [HttpPut]
        public async Task<IActionResult> Put([FromBody] CutRecordModel value)
        {
            var id = Guid.NewGuid(); // i better generate id on server side instead of client side
            var record = new RecordModel
            {
                Id = id,
                Name = value.Name,
                Image = value.Image
            };
            await _recordsRepository.AddRecordAsync(record);
            await _recordsRepository.CommitChangesAsync();
            return Ok();
        }

        // DELETE api/<RecordsCrud/1412403c-a9b7-4d2b-9e3d-60cb2beb8599
        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(Guid id)
        {
            await _recordsRepository.DeleteRecordAsync(id);
            await _recordsRepository.CommitChangesAsync();
            return Ok();
        }
    }
}
