### Readme

this is job application task.

#### How to use

There are 2 main projects inside this repo `Server` and `Client`. To run the whole system:

- Run `Server` and wait until it starts.
- Run `Client` and freely interact with it.

In case you are running from Visual Studio - set both `Server` and `Client` as startup projects under solution properties and press `F5` (or what else you press to build and run solution).

#### Note about Server

This thing uses `records.json` file which should be located in it's `bin` directory. If you want - you may use example data from [Data](Data/) directory, there are some records in it.