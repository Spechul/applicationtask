﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media.Imaging;
using Models;
using Newtonsoft.Json;

namespace ConsoleJsonFiller
{
    class Program
    {
        static async Task Main(string[] args)
        {
            /*var bitmapImage = new BitmapImage();
            bitmapImage.BeginInit();
            bitmapImage.UriSource = new Uri(Path.Combine(Environment.CurrentDirectory, "Banshee.png"));
            bitmapImage.EndInit();
            var encoder = new JpegBitmapEncoder();
            encoder.Frames.Add(BitmapFrame.Create(bitmapImage));
            byte[] data;
            using (var ms = new MemoryStream())
            {
                encoder.Save(ms);
                data = ms.ToArray();
            }

            var rawImage = JsonConvert.SerializeObject(new RecordModel
            {
                Id = Guid.NewGuid(),
                Name = "banshee",
                Image = data
            });
            //Console.WriteLine(rawImage);
            File.WriteAllText("images.json",rawImage);*/
            var client = CreateHttpClient();

            var lines = File.ReadAllLines("images.json");
            var list = new List<RecordModel>();
            foreach (var line in lines)
            {
                list.Add(JsonConvert.DeserializeObject<RecordModel>(line));
            }

            var response = await client.GetAsync("api/RecordsCrud");
            var str = await response.Content.ReadAsStringAsync();
            var array = JsonConvert.DeserializeObject<RecordModel[]>(str);
            //var httpContent = CreateRequestBody(list[0]);
            //var response1 = await client.PostAsync($"api/RecordsCrud/{Guid.NewGuid()}", httpContent);
            //var response1 = await client.PutAsync($"api/RecordsCrud", httpcontent);
            //Console.WriteLine(response1);
            Console.ReadLine();
        }

        static HttpContent CreateRequestBody<T>(T obj)
        {
            var strOut = JsonConvert.SerializeObject(obj);
            var httpContent = new StringContent(strOut, Encoding.UTF8, "application/json");
            return httpContent;
        }

        static HttpClient CreateHttpClient()
        {
            var client = new HttpClient();
            client.BaseAddress = new Uri("https://localhost:5001/");
            client.DefaultRequestHeaders.Accept.Clear();
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            return client;
        }
    }
}
