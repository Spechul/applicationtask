﻿using System;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using Newtonsoft.Json;

namespace WpfApp.Util
{
    public static class HttpClientStuff
    {
        public static HttpContent CreateRequestBody<T>(T obj)
        {
            var strOut = JsonConvert.SerializeObject(obj);
            var httpContent = new StringContent(strOut, Encoding.UTF8, "application/json");
            return httpContent;
        }

        public static HttpClient CreateHttpClient()
        {
            var client = new HttpClient();
            client.BaseAddress = new Uri("https://localhost:5001/");
            client.DefaultRequestHeaders.Accept.Clear();
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            return client;
        }
    }
}