﻿using System.IO;
using System.Windows.Media.Imaging;

namespace WpfApp.Util
{
    public static class ImageConversions
    {
        public static BitmapImage ReadImageFromBytes(byte[] bytes)
        {
            BitmapImage image = new BitmapImage();
            using (var ms = new MemoryStream(bytes))
            {
                ms.Seek(0, SeekOrigin.Begin);
                image.BeginInit();
                image.CreateOptions = BitmapCreateOptions.PreservePixelFormat;
                image.CacheOption = BitmapCacheOption.OnLoad;
                image.StreamSource = ms;
                image.EndInit();
            }

            return image;
        }

        public static byte[] WriteImageToJpegBytes(BitmapImage image)
        {
            var encoder = new JpegBitmapEncoder();
            encoder.Frames.Add(BitmapFrame.Create(image));
            byte[] data;
            using (var ms = new MemoryStream())
            {
                encoder.Save(ms);
                data = ms.ToArray();
            }

            return data;
        }

        /// <summary>
        /// just in case, but jpg files are much less in size, so they are better for demo
        /// </summary>
        /// <param name="image"></param>
        /// <returns></returns>
        public static byte[] WriteImageToPngBytes(BitmapImage image)
        {
            var encoder = new PngBitmapEncoder();
            encoder.Frames.Add(BitmapFrame.Create(image));
            byte[] data;
            using (var ms = new MemoryStream())
            {
                encoder.Save(ms);
                data = ms.ToArray();
            }

            return data;
        }
    }
}