﻿using System;
using System.ComponentModel;
using System.Diagnostics;
using System.Net.Http;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using System.Windows.Media.Imaging;
using Microsoft.Win32;
using Models;
using WpfApp.Commands;
using WpfApp.Util;

namespace WpfApp.ViewModels
{
    public class AddRecordWindowViewModel : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        #region Fields

        private Window _window;
        private ICommand _selectImageCommand;
        private ICommand _addCommand;
        private bool _nameUpdated;
        private bool _imageUpdated;

        #endregion

        #region Properties

        public RecordViewModel RecordViewModel { get; set; } = new RecordViewModel();

        public string Name
        {
            get => RecordViewModel.Name;
            set
            {
                RecordViewModel.Name = value;
                _nameUpdated = true;
                OnPropertyChanged(nameof(Name));
            }
        }

        public BitmapImage Image
        {
            get => RecordViewModel.Image;
            set
            {
                RecordViewModel.Image = value;
                _imageUpdated = true;
                OnPropertyChanged(nameof(Image));
            }
        }

        public ICommand SelectImageCommand => _selectImageCommand ?? (_selectImageCommand = new SimpleCommand(
            async () =>
            {
                await SelectImage();
            }));

        public ICommand AddCommand => _addCommand ?? (_addCommand = new SimpleCommand(
            async () =>
            {
                await AddRecord();
            },
            () => _imageUpdated && _nameUpdated));

        #endregion

        public AddRecordWindowViewModel(Window window)
        {
            _window = window;
        }

        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        #region Private Methods

        private async Task SelectImage()
        {
            Trace.WriteLine("select image command fired");
            var dialog = new OpenFileDialog
            {
                DefaultExt = ".jpg",
                Filter = "JPEG Files (*.jpg)|*.jpg"
            };
            if (dialog.ShowDialog() == true)
            {
                var bitmapImage = new BitmapImage();
                bitmapImage.BeginInit();
                bitmapImage.UriSource = new Uri(dialog.FileName);
                bitmapImage.EndInit();
                Image = bitmapImage;
                _nameUpdated = true;
            }
        }

        private async Task AddRecord()
        {
            Trace.WriteLine("add command fired");
            if (string.IsNullOrEmpty(Name))
            {
                MessageBox.Show("Name field should not be empty");
                return;
            }

            if (Image == default)
            {
                MessageBox.Show("Image field should not be empty");
                return;
            }

            HttpResponseMessage response = default;
            var client = HttpClientStuff.CreateHttpClient();
            try
            {
                var byteImage = ImageConversions.WriteImageToJpegBytes(Image);
                var content = HttpClientStuff.CreateRequestBody(new CutRecordModel()
                {
                    Name = Name,
                    Image = byteImage
                });
                response = await client.PutAsync("api/RecordsCrud", content);
            }
            finally
            {
                response?.Dispose();
                client.Dispose();
            }
            _window.Close();
        }

        #endregion

    }
}