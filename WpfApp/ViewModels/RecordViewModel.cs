﻿using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Diagnostics;
using System.IO;
using System.Net.Http;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media.Imaging;
using Microsoft.Win32;
using Models;
using WpfApp.Commands;
using WpfApp.Util;

namespace WpfApp.ViewModels
{
    public class RecordViewModel : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        #region Fields

        private Guid _id;
        private string _name;
        private BitmapImage _image;
        private ICommand _updateCommand;
        private ICommand _selectImageCommand;
        private ICommand _deleteCommand;
        private bool _updated;

        #endregion

        #region Properties

        public ObservableCollection<RecordViewModel> Holder { get; set; }

        public Guid Id
        {
            get => _id;
            set
            {
                _id = value;
                _updated = true; // in fact this can't be, but ok, why not
                OnPropertyChanged(nameof(Id));
            }
        }

        public string Name
        {
            get => _name;
            set
            {
                _name = value;
                _updated = true;
                OnPropertyChanged(nameof(Name));
            }
        }

        public BitmapImage Image
        {
            get => _image;
            set
            {
                _image = value;
                _updated = true;
                OnPropertyChanged(nameof(Image));
            }
        }

        public ICommand SelectImageCommand => _selectImageCommand ?? (_selectImageCommand = new SimpleCommand(
            async () =>
            {
                await SelectImage();
            }));

        public ICommand UpdateCommand => _updateCommand ?? (_updateCommand = new SimpleCommand(
            async () =>
            {
                await Update();
            },
            () => _updated));

        public ICommand DeleteCommand => _deleteCommand ?? (_deleteCommand = new SimpleCommand(
            async () =>
            {
                await Delete();
            }));

        #endregion

        #region Constructors

        public RecordViewModel() { }

        public RecordViewModel(Guid id, string name, BitmapImage image)
        {
            _id = id;
            _name = name;
            _image = image;
        }

        #endregion

        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        #region Private Methods

        private async Task SelectImage()
        {
            Trace.WriteLine("select image command fired");
            var dialog = new OpenFileDialog
            {
                DefaultExt = ".jpg",
                Filter = "JPEG Files (*.jpg)|*.jpg"
            };
            if (dialog.ShowDialog() == true)
            {
                var bitmapImage = new BitmapImage();
                bitmapImage.BeginInit();
                bitmapImage.UriSource = new Uri(dialog.FileName);
                bitmapImage.EndInit();
                Image = bitmapImage;
                _updated = true;
            }
        }

        private async Task Update()
        {
            Trace.WriteLine("update command fired");
            HttpResponseMessage response = default;
            var client = HttpClientStuff.CreateHttpClient();
            try
            {
                var byteImage = ImageConversions.WriteImageToJpegBytes(Image);
                var content = HttpClientStuff.CreateRequestBody(new CutRecordModel()
                {
                    Name = Name,
                    Image = byteImage
                });
                response = await client.PostAsync($"api/RecordsCrud/{Id}", content);
            }
            finally
            {
                response?.Dispose();
                client.Dispose();
            }
        }

        private async Task Delete()
        {
            Trace.WriteLine("delete command fired");
            HttpResponseMessage response = default;
            var client = HttpClientStuff.CreateHttpClient();
            try
            {
                response = await client.DeleteAsync($"api/RecordsCrud/{Id}");
                if (!response.IsSuccessStatusCode)
                {
                    // here could be throw
                    MessageBox.Show($"Cannot delete record with Id={Id}. Response code={response.StatusCode}");
                    return;
                }
                Holder?.Remove(this);
            }
            finally
            {
                response?.Dispose();
                client.Dispose();
            }
        }

        #endregion

    }
}