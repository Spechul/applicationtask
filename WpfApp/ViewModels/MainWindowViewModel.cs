﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using Models;
using Newtonsoft.Json;
using WpfApp.Commands;
using WpfApp.Util;
using WpfApp.Windows;

namespace WpfApp.ViewModels
{
    public class MainWindowViewModel : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        #region Fields

        private ObservableCollection<RecordViewModel> _recordViewModels = new ObservableCollection<RecordViewModel>();
        private ICommand _getAllCommand;
        private ICommand _addRecordWindowCommand;
        private ICommand _sortRecordsByNameCommand;

        #endregion

        #region Properties

        public ObservableCollection<RecordViewModel> RecordViewModels
        {
            get => _recordViewModels;
            set
            {
                _recordViewModels = value;
                OnPropertyChanged(nameof(RecordViewModels));
            }
        }

        public ICommand GetAllCommand => _getAllCommand ?? (_getAllCommand = new SimpleCommand(
            async () =>
            {
                await GetAll();
            }));

        public ICommand AddRecordWindowCommand => _addRecordWindowCommand ?? (_addRecordWindowCommand = new SimpleCommand(
            async () =>
            {
                await CallAddRecordWindow();
            }));

        public ICommand SortRecordsByNameCommand => _sortRecordsByNameCommand ?? (_sortRecordsByNameCommand = new SimpleCommand(
            async () =>
            {
                await SortRecordsByName();
            }));

        #endregion

        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        #region Private Methods

        private async Task GetAll()
        {
            Trace.WriteLine("get all command fired");
            HttpResponseMessage response = default;
            HttpClient client = HttpClientStuff.CreateHttpClient();
            string str = default;
            try
            {
                response = await client.GetAsync("api/RecordsCrud");
                if (!response.IsSuccessStatusCode)
                {
                    MessageBox.Show($"get all was unsuccessful, code={response.StatusCode}");
                    // technically here could be throw instead of messagebox + return
                    return;
                }
                str = await response.Content.ReadAsStringAsync();
            }
            finally
            {
                response?.Dispose();
                client.Dispose();
            }

            var records = JsonConvert.DeserializeObject<RecordModel[]>(str);
            var list = new List<RecordViewModel>();
            foreach (var record in records)
            {
                list.Add(new RecordViewModel(record.Id, record.Name, ImageConversions.ReadImageFromBytes(record.Image)));
            }

            var collection = new ObservableCollection<RecordViewModel>();
            foreach (var recordViewModel in list)
            {
                recordViewModel.Holder = collection;
                collection.Add(recordViewModel);
            }
            RecordViewModels = collection;
        }

        private async Task CallAddRecordWindow()
        {
            Trace.WriteLine("add record window command fired");
            var addWindow = new AddRecordWindow();
            var viewModel = new AddRecordWindowViewModel(addWindow);
            addWindow.DataContext = viewModel;
            addWindow.ShowDialog();
            await GetAll();
        }

        private async Task SortRecordsByName()
        {
            RecordViewModels = new ObservableCollection<RecordViewModel>(RecordViewModels.OrderBy(vm => vm.Name));
        }

        #endregion
    }
}