﻿using System;
using System.Threading.Tasks;
using System.Windows.Input;

namespace WpfApp.Commands
{
    public class SimpleCommand : ICommand
    {
        private readonly Func<Task> _action;
        private readonly Func<bool> _canExecute;

        public SimpleCommand(Func<Task> action) : this(action, null)
        {
        }

        public SimpleCommand(Func<Task> action, Func<bool> canExecute)
        {
            if (action == null)
                throw new ArgumentNullException(nameof(action));

            _action = action;
            _canExecute = canExecute;
        }

        public bool CanExecute(object parameter)
        {
            return _canExecute == null || _canExecute();
        }

        public event EventHandler CanExecuteChanged
        {
            add => CommandManager.RequerySuggested += value;
            remove => CommandManager.RequerySuggested -= value;
        }

        public async void Execute(object parameter)
        {
            await _action();
        }
    }
}