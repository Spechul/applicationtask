﻿namespace Models
{
    public class CutRecordModel
    {
        public string Name { get; set; }
        public byte[] Image { get; set; }
    }
}